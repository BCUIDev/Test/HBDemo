// /common/http.api.js
const http = uni.$u.http

// 如果没有通过拦截器配置域名的话，可以在这里写上完整的URL(加上域名部分)
let adversUrl = '/advertisingspace/advertising?space=1,2,3';
let indexUrl = '/ebapi/public_api/index';
let toutiaoNews = 'toutiao/index'
// post请求，获取菜单
export const getAdvert1 = (params, config = {}) => http.post(adversUrl, params, config)

// get请求，获取菜单，注意：get请求的配置等，都在第二个参数中，详见前面解释
export const getAdvert = (data) => http.get(adversUrl, data)

export const getNews = (data) => http.get(toutiaoNews, data)