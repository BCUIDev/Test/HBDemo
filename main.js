import App from './App'

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false

// 假设您项目中已使用VueX
// import store from './store'
// Vue.prototype.$store = store
// 引入全局uView
import uView from 'uview-ui'
Vue.use(uView)

App.mpType = 'app'
const app = new Vue({
    ...App
})
// 引入请求封装，将app参数传递到配置中
require('@/common/http.interceptor.js')(app)


app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif